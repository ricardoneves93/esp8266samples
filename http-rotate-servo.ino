// Include the Servo library
#include <Servo.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>


// Declare the Servo pin
int servoPin = D3;
// Create a servo object
Servo Servo1;
const char* ssid     = "Access Point 2_plus";
const char* password = "ricardoneves93";

// Set web server port number to 80
ESP8266WebServer server(80);

int wifiStatus;

void setup() {

  Servo1.attach(servoPin);

  Serial.begin(9600); \
  delay(200);



  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Your are connecting to;");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Your ESP is connected!");
  Serial.println("Your IP address is: ");
  Serial.println(WiFi.localIP());

  server.on("/cenas", handle_cenas);

  server.begin();

}

void loop() {
  server.handleClient();
  wifiStatus = WiFi.status();

  if (wifiStatus != WL_CONNECTED) {
    Serial.println("");
    Serial.println("WiFi not connected");
  }


}


void handle_cenas() {

  String message = "";

  if (server.arg("rotation") == "") {   //Parameter not found

    message = "Rotation not found";

  } else {    //Parameter found

    message = "Rotation Argument = ";
    message += server.arg("rotation");   //Gets the value of the query parameter

    Servo1.write(server.arg("rotation").toInt());

  }

  server.send(200, "text / plain", message);        //Returns the HTTP response
}
